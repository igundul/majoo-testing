import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color? background;
  final Border? border;
  final Function onTap;
  final Widget? icon;

  const CustomButton(
      {Key? key,
      this.icon,
      this.border,
      required this.text,
      this.background,
      required this.textColor,
      required this.onTap})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 18, horizontal: 16),
        decoration: BoxDecoration(
            border: border,
            color: background != null ? background : Color(0xff05C43D),
            borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (icon != null) ...[
              icon!,
              SizedBox(
                width: 16,
              ),
              Text(
                text,
                textAlign: TextAlign.center,
                style: GoogleFonts.inter(
                    color: textColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 14),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                  fontSize: 14,
                ),
              ),
            ] else ...[
              Expanded(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.inter(
                      color: textColor,
                      fontWeight: FontWeight.w600,
                      fontSize: 14),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                    fontSize: 14,
                  ),
                ),
              ),
            ]
          ],
        ),
      ),
    );
  }
}

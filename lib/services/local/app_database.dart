import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class AppDatabase {
  static final databaseName = "majoo";
  static final databaseVersion = 1;

  static final userTable = 'user';

  static final columnId = 'id';
  static final columnUsername = 'username';
  static final columnEmail = 'email';
  static final columnPassword = 'password';

  static final AppDatabase _singleton = AppDatabase._();

  static AppDatabase get instance => _singleton;

  Database? db;

  AppDatabase._();

  Future<Database?> get database async {
    if (db != null) return db;
    db = await _initDatabase();
    return db;
  }

  _initDatabase() async {
    final appDocumentDir = await getApplicationDocumentsDirectory();
    String path = join(appDocumentDir.path, databaseName);
    return await openDatabase(path,
        version: databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $userTable (
            $columnId INTEGER PRIMARY KEY,
            $columnUsername TEXT NOT NULL,
            $columnEmail TEXT NOT NULL,
            $columnPassword TEXT NOT NULL
          )
          ''');
  }
}

import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/local/app_database.dart';
import 'package:sqflite/sqflite.dart';

class UserDao {
  Future<Database?> get _db async => await AppDatabase.instance.database;


  Future insert(User user) async {
    try {
      final database = await _db;
      final response = await database?.insert(AppDatabase.userTable, user.toJson());
      return response;
    } catch (e) {
      print("Error Exception : ${e.toString()}");
    }
  }


  Future<User?> queryLogin(User user) async {
    try {
      final database = await _db;
      final response = await database?.query(AppDatabase.userTable,
          where: '${AppDatabase.columnEmail} = ? AND ${AppDatabase.columnPassword} = ?',
        whereArgs: [user.email, user.password]
      );

      if (response != null && response.length> 0) {
        return User.fromJson(response.first);
      }

      return null;

    } catch (e) {
      print("Error Exception : ${e.toString()}");
    }
  }
}
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/local/user_dao.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  late final UserDao userDao;

  AuthBlocCubit() : super(AuthBlocInitialState()) {
    userDao = UserDao();
  }

  void fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void getDataLogin() async {
    emit(AuthBlocProfileLoadingState());
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String? data = sharedPreferences.getString("user_value");
      if (data != null) {
        print(json.decode(data));
        User user = User.fromJson(json.decode(data));
        emit(AuthBlocProfileLoadedState(user));
      } else {
        emit(AuthBlocProfileNotLoadedState("Terjadi kesalahan"));
      }
    } catch(e) {
      print(e);
      emit(AuthBlocProfileNotLoadedState(e.toString()));
    }

  }


  void login_user(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    try {
      final response = await userDao.queryLogin(user);
      if (response != null) {
        await sharedPreferences.setBool("is_logged_in", true);
        String data = json.encode(response.toJson());
        sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocErrorState("Login gagal , periksa kembali inputan anda"));
      }
    } catch(e) {
      emit(AuthBlocErrorState(e.toString()));
    }

  }

  void register(User user) async {
    emit(AuthBlocRegisterLoadingState());
    try {
      print(user.toJson());
      await userDao.insert(user);
      emit(AuthBlocRegisterLoadedState());
    } catch(e) {
      print(e);
      emit(AuthBlocRegisterErrorState(e.toString()));
    }
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    sharedPreferences.remove("user_value");
    emit(AuthBlocLoginState());
  }
}

part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSuccessState extends AuthBlocState {}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}

//Register
class AuthBlocRegisterLoadingState extends AuthBlocState {

}

class AuthBlocRegisterErrorState extends AuthBlocState {
  final String error;

  AuthBlocRegisterErrorState(this.error);
}

class AuthBlocRegisterLoadedState extends AuthBlocState {

}

//Profile
class AuthBlocProfileLoadingState extends AuthBlocState {

}
class AuthBlocProfileLoadedState extends AuthBlocState {
  final User user;

  AuthBlocProfileLoadedState(this.user);

}
class AuthBlocProfileNotLoadedState extends AuthBlocState {
  final String string;

  AuthBlocProfileNotLoadedState(this.string);
}

import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';
import 'package:majootestcase/ui/home_bloc/component/home_header_component.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data>? data;
  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key) {
    authBlocCubit.getDataLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomRight,
              tileMode: TileMode.decal,
              colors: [Colors.black, Color(0xff141414).withOpacity(0.7)])),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          body: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                  top: 50,
                  bottom: 25,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BlocBuilder(
                      bloc: authBlocCubit,
                      builder: (context, state) {
                        String? name = "-";
                        if (state is AuthBlocProfileLoadedState) {
                          name = state.user.userName;
                          print(name);
                        }
                        return HomeHeaderComponent(
                          name: name,
                        );
                      },
                    ),

                    SizedBox(
                      height: 60,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 1.8,
                      child: Swiper(
                        itemBuilder: (BuildContext context, int index) {
                          return item(context, data![index]);
                        },
                        itemCount: data!.length,
                        viewportFraction: 0.8,
                        scale: 0.9,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Image.asset(
                        "assets/majoo.png",
                        width: 140,
                        height: 140,
                      ),
                    ),
                    // InkWell(
                    //   onTap: () {
                    //     authBlocCubit.logout();
                    //   },
                    //   child: Center(
                    //     child: Text(
                    //       "Logout",
                    //       style: GoogleFonts.nunito(
                    //         color: Colors.white
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  Widget item(context, Data data) {
    return InkWell(
      onTap: () {
        itemClicked(context, data);
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 16),
        elevation: 2,
        shadowColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(24))),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(24)),
              child: Image.network(
                data.i?.imageUrl ?? "",
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.height
              ),
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: 40,
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.6)
                  ),
                  child: Center(
                    child: Text(
                      "${data.l}",
                      style: GoogleFonts.inter(
                        color: Colors.white,
                        fontWeight: FontWeight.w600
                      ),
                    ),
                  ),
                ),
              )
            ),

          ],
        ),
      ),
    );
  }

  void itemClicked(context, data) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => DetailPage(
          data: data ,
        ),
      ),
    );
  }

}

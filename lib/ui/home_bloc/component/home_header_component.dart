

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeHeaderComponent extends StatelessWidget {
  final String? name;
  const HomeHeaderComponent({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Selamat Datang, $name 👋",
            style: GoogleFonts.nunito(
                color: Colors.white.withOpacity(0.8),
                fontWeight: FontWeight.w600,
                fontSize: 12),
          ),
          SizedBox(height: 4),
          Text(
            "lets relax, and enjoy Majoo Movie!",
            style: GoogleFonts.nunito(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/utils/snackbar_custom.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff000000),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Daftar Majoo Movie',
                  style: GoogleFonts.nunito(
                    fontSize: 24,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  'Nikmati Film Pilihan Majoo Movie Indonesia',
                  style: GoogleFonts.nunito(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.6),
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                _form(),
                SizedBox(
                  height: 24,
                ),
                BlocConsumer(
                  bloc: authBlocCubit,
                  listener: (context, state) {


                    if (state is AuthBlocRegisterLoadedState) {
                      SnackbarCustom.showSnackBar(context,
                          snackBarMode: SnackBarMode.SUCCESS,
                          content: "Register Berhasil");
                      Navigator.pop(context);
                    }

                  },
                  builder: (context, state) {
                    if (state is AuthBlocRegisterLoadingState) {
                      return Center(
                        child: LoadingIndicator(),
                      );
                    }
                    return CustomButton(
                      text: 'Register',
                      onTap: handleRegister,
                      textColor: Colors.white,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            isEmail: false,
            hint: 'Majoo',
            label: 'Username',
            labelStyle: GoogleFonts.nunito(color: Colors.white),
            hintStyle: GoogleFonts.nunito(color: Colors.white),
          ),
          SizedBox(
            height: 8,
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            labelStyle: GoogleFonts.nunito(color: Colors.white),
            hintStyle: GoogleFonts.nunito(color: Colors.white),

          ),
          SizedBox(
            height: 8,
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            labelStyle: GoogleFonts.nunito(color: Colors.white),
            hintStyle: GoogleFonts.nunito(color: Colors.white),
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() {
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

    if (_usernameController.textController.text.isNotEmpty &&
        _passwordController.textController.text.isNotEmpty &&
        _emailController.textController.text.isNotEmpty) {
      if (!pattern.hasMatch(_emailController.textController.text)) {
        SnackbarCustom.showSnackBar(context,
            snackBarMode: SnackBarMode.ERROR,
            content: "Masukkan e-mail yang valid");
        return;
      }

      authBlocCubit.register(User(
        userName: _usernameController.textController.text,
        password: _passwordController.textController.text,
        email: _emailController.textController.text,
      ));

    } else {
      SnackbarCustom.showSnackBar(context,
          snackBarMode: SnackBarMode.ERROR,
          content: "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
    }
  }
}

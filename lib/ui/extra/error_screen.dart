import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/common/widget/custom_button.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff000000),
      body: SafeArea(
        child: message == "Network"
            ? networkWidget()
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      message,
                      style: GoogleFonts.nunito(
                          fontSize: 12, color: textColor ?? Colors.white),
                    ),
                    retry != null
                        ? Column(
                            children: [
                              SizedBox(
                                height: 100,
                              ),
                              retryButton ??
                                  IconButton(
                                    onPressed: () {
                                      if (retry != null) retry!();
                                    },
                                    icon: Icon(Icons.refresh_sharp),
                                  ),
                            ],
                          )
                        : SizedBox()
                  ],
                ),
              ),
      ),
    );
  }

  networkWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Icon(
            CupertinoIcons.wifi_slash,
            color: Colors.white,
            size: 62,
          ),
        ),
        SizedBox(height: 24),
        Center(
          child: Text(
            "Terjadi Kesalahan",
            style: GoogleFonts.inter(
              color: Colors.white,
              fontSize: 21
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(24),
          child: CustomButton(
              text: 'Refresh',
              onTap: () {
                if (retry != null) retry!();
              },
              textColor: Colors.white),
        )
      ],
    );
  }
}

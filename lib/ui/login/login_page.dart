import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:majootestcase/utils/snackbar_custom.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff000000),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: GoogleFonts.nunito(
                    fontSize: 24,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: GoogleFonts.nunito(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.6),
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                _form(),
                SizedBox(
                  height: 24,
                ),
                BlocConsumer(
                  bloc: authBlocCubit,
                  listener: (context, state) {
                    if (state is AuthBlocLoggedInState) {
                      SnackbarCustom.showSnackBar(context,
                          snackBarMode: SnackBarMode.SUCCESS,
                          content: "Login Berhasil");

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => BlocProvider(
                            create: (context) => HomeBlocCubit()..fetching_data(),
                            child: HomeBlocScreen(),
                          ),
                        ),
                      );
                    }

                    if (state is AuthBlocErrorState) {
                      SnackbarCustom.showSnackBar(context,
                          snackBarMode: SnackBarMode.ERROR,
                          content: state.error);
                    }
                  },
                  builder: (context, state) {
                    if (state is AuthBlocLoadingState) {
                      return Center(
                        child: LoadingIndicator(),
                      );
                    }

                    return CustomButton(
                        text: 'Login',
                        onTap: handleLogin,
                        textColor: Colors.white);
                  },
                ),
                SizedBox(
                  height: 32,
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            labelStyle: GoogleFonts.nunito(color: Colors.white),
            hintStyle: GoogleFonts.nunito(color: Colors.white),
          ),
          SizedBox(
            height: 8,
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            labelStyle: GoogleFonts.nunito(color: Colors.white),
            hintStyle: GoogleFonts.nunito(color: Colors.white),
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: navigateToRegister,
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.white),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void navigateToRegister() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => RegisterPage(),
      ),
    );
  }

  void handleLogin() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      if (!pattern.hasMatch(_email)) {
        SnackbarCustom.showSnackBar(context,
            snackBarMode: SnackBarMode.ERROR,
            content: "Masukkan e-mail yang valid");
        return;
      }
      User user = User(
        email: _email,
        password: _password,
      );
      authBlocCubit.login_user(user);
    }
  }
}

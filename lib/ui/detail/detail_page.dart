import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/constant.dart';

class DetailPage extends StatefulWidget {
  final Data data;

  const DetailPage({Key? key, required this.data}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Data get data => widget.data;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomRight,
              tileMode: TileMode.decal,
              colors: [Colors.black, Color(0xff141414).withOpacity(0.7)])),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          body: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                padding:
                    EdgeInsets.only(top: 50, bottom: 25, left: 16, right: 16 ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        CupertinoIcons.chevron_back,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(24)),
                            child: Image.network(
                              data.i?.imageUrl ?? "",
                              fit: BoxFit.fill,
                              height: MediaQuery.of(context).size.height / 2.3,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            padding: EdgeInsets.only(left: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _rectangleComponent(
                                  CupertinoIcons.video_camera_solid,
                                  "Genre",
                                  "${data.q}",
                                ),
                                SizedBox(height: 24,),
                                _rectangleComponent(
                                  CupertinoIcons.star_fill,
                                  "Rank",
                                  "${data.rank}",
                                ),
                                SizedBox(height: 24,),
                                _rectangleComponent(
                                  CupertinoIcons.calendar_circle_fill,
                                  "Tahun",
                                  "${data.year}",
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    Text(
                      "${data.l}",
                      style: GoogleFonts.inter(
                        color: Colors.white,
                        fontSize: 26,
                        fontWeight: FontWeight.w700
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Divider(color: Colors.white.withOpacity(0.4),),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Sipnosis",
                      style: GoogleFonts.nunito(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                    SizedBox(height: 8,),
                    Text(
                      "${TextConstant.loremIpsum}",
                      style: GoogleFonts.nunito(
                          color: Colors.white.withOpacity(0.7),
                          fontSize: 14,
                          fontWeight: FontWeight.w400
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  _rectangleComponent(IconData icon, String title, String content) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white, width: 0.3),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        children: [
          Icon(
            icon,
            color: Colors.white,
          ),
          SizedBox(height: 4,),
          Text(
            "$title",
            style: GoogleFonts.inter(
              color: Colors.white.withOpacity(0.4),
              fontSize: 12
            ),
          ),
          SizedBox(height: 6),
          Text(
            "$content",
            style: GoogleFonts.inter(
              color: Colors.white,
              fontSize: 12
            ),
          ),
        ],
      ),
    );
  }
}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

enum SnackBarMode {
  ERROR,
  SUCCESS
}

class SnackbarCustom {
  static showSnackBar(BuildContext context, {
    required SnackBarMode snackBarMode,
    required String content,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        content,
        style: GoogleFonts.inter(
            color: snackBarMode == SnackBarMode.SUCCESS ? Color(0xff22AA5F) : Color(0xffB61616)
        ),
      ),
      duration: Duration(milliseconds: 6000),
      backgroundColor: snackBarMode == SnackBarMode.SUCCESS ? Color(0xffAAEEC9) : Color(0xffFFB7B7),
      behavior: SnackBarBehavior.floating,
      shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(9)
      ) ,
    ));
  }

}
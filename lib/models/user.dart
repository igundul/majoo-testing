class User {
  String? email;
  String? userName;
  String? password;

  User({this.email, this.userName, this.password});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        userName = json['username'],
        password = json['password'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'username': userName};
}
